#! /usr/bin/env python3
''' Print tasks, filtering by owner and status. '''

import argparse
import datetime
import functools
import glob
import os
import re
import subprocess
import sys
from contextlib import contextmanager

import lark
from clint.textui import colored

import __main__
import zorg.autoformatter
import zorg.parser


def is_daily_log(filename):
    '''
    >>> bool(is_daily_log('2021-02-23.md'))
    True
    >>> bool(is_daily_log('2021-02-23mmd'))
    False
    '''
    return re.match(r'\d{4}-\d{2}-\d{2}\.md$', filename)

def is_project_log(filename):
    '''
    >>> bool(is_project_log('2022-01-06-schematic-pghba-objects.md'))
    True
    >>> bool(is_project_log('2021-11-06-migrate-play_ranking_snapshot-2.md'))
    True
    >>> bool(is_project_log('2022-01-06-test.md'))
    True
    >>> bool(is_project_log('2022-01-06-test-md'))
    False
    >>> bool(is_project_log('2022-01-06.md'))
    False
    '''
    return re.match(r'\d{4}-\d{2}-\d{2}-[\w0-9_-]{1,}\.md$', filename)

@contextmanager
def pager(disabled=False):
    if disabled:
        yield
        return
    ttyconf = subprocess.check_output(['stty', '-g']).strip()
    cmd = ['less', '-SR']
    proc = subprocess.Popen(cmd, stdin=subprocess.PIPE, text=True)
    stdout, stderr = sys.stdout, sys.stderr
    sys.stdout, sys.stderr = proc.stdin, proc.stdin
    proc.stdin.isatty = stdout.isatty
    try:
        yield proc
        proc.communicate()
    finally:
        proc.kill()
        sys.stdout, sys.stderr = stdout, stderr
        subprocess.check_call(['stty', ttyconf])

def search_tasks_recursive(tasks, pred, args):
    if not tasks:
        return []
    return [{**t, **{
        'subtasks': search_tasks_recursive(t['subtasks'], pred, args) if args.depth else t['subtasks']}
        } for t in tasks if pred(t)]

def task_pred(args, task):
    ''' Predicate for matching tasks based on cli args. '''
    # TODO: support boolean queries with parenthesis and ORs
    if args.status and (task['status'] or '') not in args.status:
        return False
    formatted_task = zorg.autoformatter.format_item(task).lower()
    if args.query and any([q not in formatted_task for q in args.query.lower().split(' ')]):
        return False
    return True

def print_output(args):
    root = os.environ['ZORG_DIR']
    max_date = datetime.datetime.now().date() + datetime.timedelta(days=args.peek_ahead)
    max_filename = '%s.md' % max_date.strftime('%Y-%m-%d')
    for filepath in sorted(glob.glob('%s/log/*.md' % root), reverse=True):
        filename = os.path.basename(filepath)
        if args.ignore_projects:
            if not is_daily_log(filename):
                continue
        else:
            if not (is_daily_log(filename) or is_project_log(filename)):
                continue
        if filename > max_filename:
            continue
        if not os.path.exists(filepath):
            continue
        if not os.stat(filepath).st_blocks:
            continue
        content = open(filepath).read()
        try:
            data = zorg.parser.parse(content)
        except (lark.exceptions.UnexpectedCharacters, lark.exceptions.UnexpectedToken) as ex:
            sys.stdout.write('{:-^75s}\n'.format(' ' + filename + ' (parsing error) '))
            if args.verbose:
                print(colored.red(str(ex)))
                print('Context:')
                print(colored.yellow(ex.get_context(content)))
            continue
        except (zorg.parser.IndentError, zorg.parser.DedentError) as ex:
            print('%s: %s' % (filename, ex), file=sys.stderr)
            continue
        lines = []
        for section, tasks in data.items():
            if args.owner and args.owner != section:
                continue
            matching_tasks = search_tasks_recursive(tasks, functools.partial(task_pred, args), args)
            if not matching_tasks:
                continue
            lines.append('\n# %s\n\n---\n' % section)
            for task in matching_tasks:
                lines.append('%s\n' % zorg.autoformatter.format_item(task))
            lines.append('---\n\n')
        if lines:
            sys.stdout.write('{:-^75s}\n'.format(' ' + filename + ' '))
            for line in lines:
                sys.stdout.write(line)
        sys.stdout.flush()

def main():
    parser = argparse.ArgumentParser(description=__main__.__doc__)
    parser.add_argument('-o', '--owner', help='filter by owner ("free", "your name")')
    parser.add_argument('-s', '--status', default=None, help='filter by status [.x pcw*] etc')
    parser.add_argument('-q', '--query', default=None, help='filter by status [.x pcw*] etc')
    parser.add_argument('-d', '--depth', action='store_true', help='apply filter to subtasks as well')
    parser.add_argument('-p', '--peek-ahead', default=7, type=int, help='look-ahead by this many days')
    parser.add_argument('-n', '--no-pager', action='store_true', help='do not open in pager')
    parser.add_argument('-i', '--ignore-projects', action='store_true', help='ignore project files')
    parser.add_argument('-v', '--verbose', action='store_true', help='print parsing errors')
    args = parser.parse_args()
    isatty = os.isatty(sys.stdout.fileno())
    if args.no_pager == False and not isatty: # we are not in a tty, disable pager
        args.no_pager = True
    with pager(disabled=args.no_pager):
        try:
            print_output(args, )
        except BrokenPipeError:
            return

if __name__ == '__main__':
    main()
