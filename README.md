## Overview

Zorg is a todo system for busy people. Collaborate with yourself or your team with todo lists in markdown files.

Benefits:
* Use your keyboard and your favorite text editor instead of clicking around a slow web interface.
* Simple and intuitive format, with low barrier to entry compared to org mode. No editor plug in necessary.
* Aggregate a single curated list of todos from everywhere else: email, calendar, bug trackers, support tickets, physical mail, etc.
* Lower stress by working through todo lists for a single day at a time instead of increasingly long and impossible to manage lists.
* Communicating what you're working on with the team without having tedious stand-up meetings, calls, or chat logs.
* Easily see what others are working on today, what's blocked, what is finished, and what is paused.
* Put reminders on future days that you'll see when you start work that day.
* Offline first, synced at your own pace.
* Retain history of all todos over all time with a full audit log of every change.
* CLI tooling for querying tasks, summary reports, and workflow automation.


## Workflow

Each day, a new markdown file is created at `log/YYYY-MM-DD.md` with the day's date.

Tasks are added to the file using using the structure described below. They are ordered from top to bottom in the intended chronological order that they are to be done. Tasks can be scheduled for the current day as the day evolves, or for any future day by creating the appropriate `log/YYYY-MM-DD.md` file with the future date.

Tasks are marked with `[ ]` to indicate they haven't been started yet, `[.]` to indicate they have been started, and `[x]` when they are finished. Other standard task status are described below.

After a day is over, unstarted tasks may remain. The recommendation is to either move them to a future day when they might be done, or to just leave them there for now until there's motivating to make time for them. Don't default to moving tasks to tomorrow, because that's how you end up with an increasingly long todo list that's impossible to manage. It's OK that not everything is done— it's better to be able to add stretch goals and let them fade into history if you don't get to them.

Other tasks may be started but unfinished. Mark them as paused (`[p]`) until you can start them back up. When you continue the task, copy it to the current/future day, remove the subtasks that were already finished, and keep just the in-progress and unstarted subtasks.

The caveat to continuing the task on a future day is that you or someone else might see a task on a previous day marked as paused, and think that it was neglected. To prevent this, mark the previous version as continued (`[c]`) to indicate it was continued on a future day. Also mark the in-progress sub-tasks as continued, and delete the unstarted sub-tasks. If this is confusing, just think about someone else reviewing the previous version, and how it should look so they don't get the impression something was left behind.

Work is saved and synchronized with git. Commits should be _fast_ and _frequent_; don't use commit messages, the changes speak for themselves. Use the `zorg-commit` command to commit. Use the `zorg-sync` command to commit, pull, and push. Sync whenever the status of a task changes. Use your discretion to skip syncing minor sub-tasks.

The formatting of markdown files is validated on every commit with a git pre-commit hook. Add more hooks under `.githooks/pre-commit/` for any additional validation.


## Structure

In each markdown file, there's one section per person (headed with `#` and person's name), each section is a list of tasks, one per line. Three dashes before and after tasks (to aid markdown formatting).

Notes about a task can be written as bullets under a task (denoted with an indent and a dash "-"), or auxilliary files in the log, eg (2020-12-31-foo.md, 2020-12-31-bar.sql, ...), referenced by filepath in a task.


```

# scott

---
[ ] unstarted task
[.] started task
[x] finished task
[c] started but unfinished task, moved to another day (otherwise it looks like the tasks are forgotten/incomplete)
[-] task that will not be performed, archived/abandoned
[w] waiting/blocked
[p] paused
[s] sleep (paused indefinitely)
[f] a failed task (attempted and failed, abandoned)
[^] urgent task
[ ] parent task
    [ ] child task (use 4 space indentation, not tabs, not 2 space)
        - a note about what I found while looking into this
[ ] {foo} a tagged task
---

## a failed task (attempted and failed, abandoned)

Tried to integrate, but the API no longer exists.

```



## Tags

Some example tags. Useful for searching, analyzing, contextualizing, etc.

```
{plan}              planning/strategizing/preparing
{pm}                project management
{bi}                business intelligence/research/competitive intelligence
{learn}             learning/self-education/etc
{meeting}           a meeting
{ops}               operational/admin work
{dba}               database administration
{eng}               engineering
```

## Setup

To setup:
1. fork or clone this repo (`git clone git@gitlab.com:deltaex/zorg.git`)
2. open the directory: `cd zorg`
3. install nix: `sh shell/nix-install.sh`
4. run `nix-shell` (build may take a bit on first run)
5. (optional) open a tmux server (`tmux -L zorg new`) (useful for using multiple shells without having to call `nix-shell` each time)
6. Create a new branch for your content, that way the "zorg" branch can be used for pulling updates and contributing back to the project itself. The shared/common branch is "zorg". Eg: `git branch main ; git checkout main`.


## Commands

```
zorg-sync                    # synchronize with remote by doing a commit, pull, and push
zorg-commit                  # add and commit all outstanding changes with empty commmit message
zorg-pull                    # pull changes
zorg-status                  # show all tasks (looking ahead up to --peek-ahead 7 days)
zorg-status -o free          # show free tasks
zorg-status -o scott -s 'x'  # show tasks completed by scott
zorg-status -q '{plan}'      # show tasks containing text "{plan}"
zorg-status -q 'setup'       # show tasks containing text "setup"
zorg-status -s ' '           # show todo tasks
zorg-status -s 'p'           # show paused tasks
zorg-status -s '.'           # show active tasks
zorg-status -s 'pw.'         # show started tasks
zorg-spaced-repetition       # used spaced repetition to review all history in small chunks over the course of the year
```
