from lark import Lark, Transformer
from lark.indenter import Indenter
from lark.exceptions import UnexpectedCharacters, UnexpectedToken
import IPython


def parse(text):

    zorg_grammar = """
    SYMBOL: /[^\\r\\n]/
    STRING: SYMBOL+
    FOUR_SPACES: "    "
    _MARKER: "---" _NEWLINE

    DEV: STRING
    _header: "# " DEV _NEWLINE

    STATUS: "[" /[^\\r\\n\\]]*/ "]"
    TAG: "{" _WS* /[a-z]+/ _WS* "}" _WS*
    tags: TAG*
    DESCRIPTION: /[^\\r\\n]+/
    _NEWLINE: " "* _NEWLINE_INTERNAL " "*
    tree: STATUS _WS* tags _WS* DESCRIPTION _NEWLINE [_INDENT tree+ _DEDENT] -> task
        | "#" "#" _WS* DESCRIPTION _NEWLINE [_INDENT tree+ _DEDENT] -> section
        | "-" _WS* DESCRIPTION _NEWLINE [_INDENT tree+ _DEDENT] -> note

    personal_list: _NEWLINE* _header _MARKER tree* _MARKER
    list_of_lists: personal_list+

    %declare _INDENT _DEDENT
    %import common.NEWLINE -> _NEWLINE_INTERNAL
    %import common.WS -> _WS
    """

    class TreeIndenter(Indenter):
        NL_type = '_NEWLINE'
        OPEN_PAREN_types = []
        CLOSE_PAREN_types = []
        INDENT_type = '_INDENT'
        DEDENT_type = '_DEDENT'
        tab_len = 4

    class ZorgTransformer(Transformer):

        def tags(self, args):
            return [str(''.join(x for x in i if x.isalpha())) for i in args]

        def section(self, args):
            description = args[0]
            subtasks = args[1:]
            return {
                'description': str(description),
                'tags': [],
                'status': None,
                'subtasks': subtasks,
                'extra_data': {
                    'is_section': True
                    }
            }

        def note(self, args):
            description = args[0]
            subtasks = args[1:]
            return {
                'description': str(description),
                'tags': [],
                'status': None,
                'subtasks': subtasks,
                'extra_data': {
                    'is_note': True
                    }
            }

        def task(self, args):
            status_raw = args[0]
            tags = args[1]
            description = args[2]
            subtasks = args[3:]
            status_char = " "
            # get the char if there's a non-space char in status_raw, otherwise space
            for i in status_raw[1:-1]:
                if i != ' ':
                    status_char = i
            return {
                'description': str(description),
                'tags': tags,
                'status': status_char,
                'subtasks': subtasks,
                'extra_data': {}
            }

        def personal_list(self, args):
            return (str(args[0]), args[1:])

        def list_of_lists(self, args):
            return dict(args)
        
    my_parser = Lark(zorg_grammar, start='list_of_lists', parser='lalr', postlex=TreeIndenter())
    parse_results = my_parser.parse(text)
    transform_results = ZorgTransformer().transform(parse_results)
    return transform_results
