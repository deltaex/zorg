#! /usr/bin/env python3
''' Create future empty daily files from a template. '''

import argparse
import datetime
import os
import sys

import __main__


def is_empty(filepath):
    try:
        stat = os.stat(filepath)
        return stat.st_size == 0
    except FileNotFoundError:
        return True

def main():
    parser = argparse.ArgumentParser(description=__main__.__doc__)
    parser.add_argument('-t', '--template', default='log/template.md', help='filename of template file to use')
    parser.add_argument('-n', '--num-days', default=7, type=int, help='create files this many days ahead')
    args = parser.parse_args()
    try:
        content = open(args.template).read() if args.template else ''
    except FileNotFoundError:
        print('file not found %s' % args.template, file=sys.stderr)
        sys.exit(1)
    today = datetime.datetime.now().date()
    for i in range(args.num_days + 1):
        day = today + datetime.timedelta(days=i)
        daily_filename = '%s.md' % day.strftime('%Y-%m-%d')
        monthly_filename = '%s-01-month.md' % day.strftime('%Y-%m')
        for filename in (daily_filename, monthly_filename):
            filepath = os.path.join(os.environ['ZORG_DIR'], 'log', filename)
            if is_empty(filepath):
                with open(filepath, 'w') as fp:
                    fp.write(content)
                print('created %s' % filename, file=sys.stderr)

if __name__ == '__main__':
    main()
