{ pkgs ?
  (import (builtins.fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/2992f686ea49cdf7b89a6092b08fa14e4d56efd3.tar.gz";
        sha256 = "1wfxkymcclx07q9jrb3lyji31fxsq9ynf5r1rwlr68iw45s1crwf";
  }) { }) }:
let
  stdenv = pkgs.stdenv;
in pkgs.mkShell rec {
    name = "zorg";
    shellHook = ''
        source ./shell/.bashrc
    '';
    py3 = pkgs.python38.override {
        packageOverrides = self: super: with self; {

        };
    };
    buildInputs = (with pkgs; [
        (git.override { openssh = openssh_with_kerberos; })
        broot
        bashInteractive
        curl
        glibcLocales
        glow
        gnugrep
        hostname
        htop
        less
        man
        nix-prefetch-scripts
        tig
        tmux
        tree
        which
        (py3.buildEnv.override {
            ignoreCollisions = true;
            extraLibs = with py3.pkgs; [
                (if stdenv.isLinux then pylint else null)
                clint
                ipython
                isort
                lark
                nose
                pudb
            ];
        })
    ]);
}
