alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'
alias sort='LC_ALL=C sort'
alias git='PAGER=less LESS="-RiMSx4 -FX" git'
alias grp='grep -P --color=always'
alias man='PAGER=less LESS="-RiMSx4" man'
alias isort="isort --settings-path $ZORG_DIR/etc/.isort.cfg"
alias nose="nosetests --config $ZORG_DIR/etc/nose/nose.cfg"
alias lint='pylint --rcfile=$ZORG_DIR/etc/pylint/default.rc'

alias zorg-commit='git add . && git commit --allow-empty-message -m " " && GIT_PAGER=cat git show HEAD'
alias zorg-pull='git pull --rebase'
alias zorg-push='git push'
alias zorg-sync='zorg-init-days && (zorg-commit || true) && zorg-pull && zorg-push'
alias zorg-idle='git log --date=relative -n1 --format="last commit: %Cred%cd"'
alias zorg-status='$ZORG_DIR/src/zorg/status.py'
alias zorg-check-syntax='$ZORG_DIR/src/zorg/check_syntax.py'
alias zorg-init-days='$ZORG_DIR/src/zorg/init_days.py'
alias zorg-today='$EDITOR $ZORG_DIR/log/`date +"%Y-%m-%d"`.md'


[ -f ~/.bash_aliases ] && source ~/.bash_aliases
unset command_not_found_handle  # looks for a command when not found in path; slow
