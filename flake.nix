{
  description = "Zorg's flake";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/2992f686ea49cdf7b89a6092b08fa14e4d56efd3";
  };

  outputs = { self, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
        in
        {
          devShells.default = import ./default.nix {
            inherit pkgs;
          };
        }
      );
}
