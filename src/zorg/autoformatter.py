import json
from pprint import pformat
from sys import argv

from zorg.parser import parse

INDENT = '    '


def format_item(item):
    if not item:
        return ''
    if item['extra_data'].get('is_section'):
        return '\n## {}\n'.format(item['description'])
    if item['extra_data'].get('is_note'):
        return '    - {}'.format(item['description'])
    return '[{status}]{tags} {description}{subtasks}'.format(
        status=item['status'],
        tags=' '.join(['']+['{{{}}}'.format(tag) for tag in item['tags']]),
        description=item['description'],
        subtasks=(''.join(['\n'+format_item(subitem) for subitem in item['subtasks']]))).replace('\n', '\n    ')


def format_personal_list(personal_list):
    return '---\n\n# {}\n\n---\n{}'.format(personal_list[0], '\n'.join([format_item(i) for i in personal_list[1]]))


def format_list(text):
    todo_dict = parse(text)
    lists = todo_dict.items()
    return '\n'.join(format_personal_list(entry) for entry in lists)


def format_file(filename):
    with open(filename, 'r') as infile:
        print(format_list(infile.read()))


if __name__ == "__main__":
    format_file(argv[1])
