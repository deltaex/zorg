#!/bin/sh

# This script installs the Nix package manager on your system by
# downloading a binary distribution and running its installer script
# (which in turn creates and populates /nix).

{ # Prevent execution if this script was only partially downloaded
oops() {
    echo "$0:" "$@" >&2
    exit 1
}

umask 0022

tmpDir="$(mktemp -d -t nix-binary-tarball-unpack.XXXXXXXXXX || \
          oops "Can't create temporary directory for downloading the Nix binary tarball")"
cleanup() {
    rm -rf "$tmpDir"
}
trap cleanup EXIT INT QUIT TERM

require_util() {
    command -v "$1" > /dev/null 2>&1 ||
        oops "you do not have '$1' installed, which I need to $2"
}

case "$(uname -s).$(uname -m)" in
    Linux.x86_64)
        hash=5e6ad6b2129c6906a40cc49252fe252fbfb5612fea3c614af17cb194ba600c08
        path=mhdwlzdyasip9f3n2931064q3vyijlhz/nix-2.20.4-x86_64-linux.tar.xz
        system=x86_64-linux
        ;;
    Linux.i?86)
        hash=3e8b6190973db43f1f32ab727cbd27348e7a5f1e49dce3d60f0f9aaf735891a6
        path=zkcnqs0ixh4xra22l5kqq8pa8n80yk5f/nix-2.20.4-i686-linux.tar.xz
        system=i686-linux
        ;;
    Linux.aarch64)
        hash=187411e2a327c5f4d8d90c3be6a57b2d32eef78cc84a41d0a098c25a6c2bd382
        path=0bqvcj048knc4bcdpzp0w1v7dsxwyj3j/nix-2.20.4-aarch64-linux.tar.xz
        system=aarch64-linux
        ;;
    Linux.armv6l)
        hash=3287e86ddc03529a2e2ca96aff78cd5381e5955a07a3ffb0b786a28cf19ef47f
        path=m1dfbwgh3bm0bwxzn5i5ail62ya1ar1h/nix-2.20.4-armv6l-linux.tar.xz
        system=armv6l-linux
        ;;
    Linux.armv7l)
        hash=4e3c872cdee6011507deaac54e874adda76ce8ff37026a74804a5ae2cdfde944
        path=57hqrr3qqd9qif0v2hm12wjyaimqxy5x/nix-2.20.4-armv7l-linux.tar.xz
        system=armv7l-linux
        ;;
    Darwin.x86_64)
        hash=65afb655c6906ccd85424d5b95472902b1e0bdbcf1d6e2411a6863df57842274
        path=z476mhxmkgvw53xz0wiv56ivy85bw96c/nix-2.20.4-x86_64-darwin.tar.xz
        system=x86_64-darwin
        ;;
    Darwin.arm64|Darwin.aarch64)
        hash=4725c661df9b6a1010b6e104af1cab213b093299b9570b67594070ecbb18de48
        path=q4ahl3y68jvzjxa6p53z2032vrh4plyy/nix-2.20.4-aarch64-darwin.tar.xz
        system=aarch64-darwin
        ;;
    *) oops "sorry, there is no binary distribution of Nix for your platform";;
esac

# Use this command-line option to fetch the tarballs using nar-serve or Cachix
if [ "${1:-}" = "--tarball-url-prefix" ]; then
    if [ -z "${2:-}" ]; then
        oops "missing argument for --tarball-url-prefix"
    fi
    url=${2}/${path}
    shift 2
else
    url=https://releases.nixos.org/nix/nix-2.20.4/nix-2.20.4-$system.tar.xz
fi

tarball=$tmpDir/nix-2.20.4-$system.tar.xz

require_util tar "unpack the binary tarball"
if [ "$(uname -s)" != "Darwin" ]; then
    require_util xz "unpack the binary tarball"
fi

if command -v curl > /dev/null 2>&1; then
    fetch() { curl --fail -L "$1" -o "$2"; }
elif command -v wget > /dev/null 2>&1; then
    fetch() { wget "$1" -O "$2"; }
else
    oops "you don't have wget or curl installed, which I need to download the binary tarball"
fi

echo "downloading Nix 2.20.4 binary tarball for $system from '$url' to '$tmpDir'..."
fetch "$url" "$tarball" || oops "failed to download '$url'"

if command -v sha256sum > /dev/null 2>&1; then
    hash2="$(sha256sum -b "$tarball" | cut -c1-64)"
elif command -v shasum > /dev/null 2>&1; then
    hash2="$(shasum -a 256 -b "$tarball" | cut -c1-64)"
elif command -v openssl > /dev/null 2>&1; then
    hash2="$(openssl dgst -r -sha256 "$tarball" | cut -c1-64)"
else
    oops "cannot verify the SHA-256 hash of '$url'; you need one of 'shasum', 'sha256sum', or 'openssl'"
fi

if [ "$hash" != "$hash2" ]; then
    oops "SHA-256 hash mismatch in '$url'; expected $hash, got $hash2"
fi

unpack=$tmpDir/unpack
mkdir -p "$unpack"
tar -xJf "$tarball" -C "$unpack" || oops "failed to unpack '$url'"

script=$(echo "$unpack"/*/install)

[ -e "$script" ] || oops "installation script is missing from the binary tarball!"
export INVOKED_FROM_INSTALL_IN=1
"$script" "$@"

} # End of wrapping
