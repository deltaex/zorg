#! /usr/bin/env python3
''' Print tasks, filtering by owner and status. '''

import glob
import os
import re
import subprocess
import sys
from pprint import pprint

import lark
from clint.textui import colored

import zorg.autoformatter
import zorg.parser


def is_daily_log(filename):
    '''
    >>> not not is_daily_log('2021-02-23.md')
    True
    >>> not not is_daily_log('2021-02-23-arbitrary-text.md')
    True
    >>> not not is_daily_log('2021-02-23-arbitrary_text.md')
    True
    '''
    return re.match(r'\d{4}-\d{2}-\d{2}(?:-[^.]*)?.md$', filename)

def main():
    if '--all' in sys.argv:
        zorgfiles = glob.glob('%s/log/*.md' % os.environ['ZORG_DIR'])
    elif sys.argv[1:]:
        zorgfiles = sys.argv[1:]
    else:
        zorgfiles = subprocess.check_output(
            ['git', 'diff', '--cached', '--diff-filter', 'ACMR', '--name-only', 'log/'], text=True).split('\n')
    for filepath in sorted(zorgfiles, reverse=True):
        filename = os.path.basename(filepath)
        if not is_daily_log(filename):
            continue
        if not os.path.exists(filepath):
            continue
        if not os.stat(filepath).st_blocks:
            continue
        content = open(filepath).read()
        try:
            parsed = zorg.parser.parse(content)
            if 'ZORG_DEBUG' in os.environ:
                pprint(parsed)
        except (lark.exceptions.UnexpectedCharacters, lark.exceptions.UnexpectedToken) as ex:
            print('%s invalid syntax' % filename, file=sys.stderr)
            print(colored.red(str(ex)), file=sys.stderr)
            print('Context:', file=sys.stderr)
            print(colored.yellow(ex.get_context(content)), file=sys.stderr)
            sys.exit(1)

if __name__ == '__main__':
    main()
