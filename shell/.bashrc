#! /usr/bin/env bash
HISTLOGPATH="$HOME/.history/$(hostname)/$(date -u +%Y/%m)"
HISTLOGFILE=$HISTLOGPATH/$(date -u +%dd.%Hh.%Mm.%Ss)_bash-$$
mkdir -p $HISTLOGPATH
if [ ! -z "$shellHook" ]; then
    # we just built a new nix-env, cache it for later use
    export ZORG_DIR=$PWD
    unset shellHook
    export ENV_FILE="$ZORG_DIR"/.zorg.env
    export -p > $ENV_FILE
    $ZORG_DIR/.githooks/autohook.sh install  # install git hooks
else
    # we are entering a nix-env from a plain bash env or an old nix-env that should be refreshed
    cached_env=$(ls .zorg.env 2>/dev/null || printf "")
    [ -n "$cached_env" ] && source $cached_env && export ENV_FILE="$cached_env" && cd $ZORG_DIR
    unset latest_env
fi
chmod +x $ZORG_DIR/shell/.bashrc $ZORG_DIR/shell/nixbash $ZORG_DIR/shell/.bash_shopt $ZORG_DIR/shell/.bash_aliases $ZORG_DIR/.githooks/autohook.sh
export PYTHONPATH=$ZORG_DIR/src:$PYTHONPATH
export PATH=$ZORG_DIR/shell:$ZORG_DIR/bin:$PATH
export PS1='\[\033[01;91m\](zorg)\[\033[00m\] \[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
unset TZ
export SHELL="$ZORG_DIR/shell/nixbash"
export HISTCONTROL=ignoredups:ignorespace
export HISTFILESIZE=1000000000                  # max lines in bash history file
export HISTSIZE=1000000000                      # max lines in bash history per session
export EDITOR=${EDITOR:-vim}
[ -f /etc/bash_completion ] && ! shopt -oq posix && source /etc/bash_completion
[ -x /usr/bin/lesspipe ] && eval "$(/usr/bin/lesspipe)" # enable less to view non-textual files
[ -x /usr/bin/dircolors ] && eval "$(/usr/bin/dircolors -b)" # enable color support
[ -f $ZORG_DIR/shell/.bash_aliases ] && source $ZORG_DIR/shell/.bash_aliases
[ -f $ZORG_DIR/shell/.bash_shopt ] && source $ZORG_DIR/shell/.bash_shopt
export TERM=screen-256color
export PG_COLOR=auto
export PSQL_HISTORY="$HISTLOGFILE""-psql"
export IPYTHON_HISTORY="$HISTLOGFILE""-ipython"
export IPYTHONDIR=$ZORG_DIR/.ipython/
promptFunc(){
    history -a  # flush to history file
    # audit log of all bash commands
    echo "$(date +%Y-%m-%d--%H-%M-%S) $PWD $(history 1)" >> $HISTLOGFILE
}
PROMPT_COMMAND=promptFunc
# source any local customizations
[ -f $ZORG_DIR/shell/.bashrc-local ] && source $ZORG_DIR/shell/.bashrc-local

## create a symlink between this nix-shell and the nix gcroots
savedrv(){
	if [ "$out" ];  then
		drv=$(nix show-derivation $out | python -c 'import sys,json; print(list(json.load(sys.stdin).keys())[0])')
		if [ "$drv" ] && [ -e "$drv" ]; then
			ln -fs "$drv" $ZORG_DIR/.drv
			ln -fs "$ZORG_DIR/.drv" "/nix/var/nix/gcroots/per-user/$LOGNAME/mixrank-$(basename "$ZORG_DIR")"
		fi
	fi
}
